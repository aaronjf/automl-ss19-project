import numpy as np
import torch
import torch.nn.functional as F
import torch.nn as nn
from torch.autograd import Variable
from tqdm import tqdm
import time

import logging

from utils import AvgrageMeter, accuracy


class torchModel(nn.Module):
    def __init__(self, config, input_shape=(1, 28, 28), num_classes=10):
        super(torchModel, self).__init__()
        # config: {
        #   conv1: { out_channels, kernel_size, stride, padding },
        #   # pool1_type,
        #   # pool1: { kernel_size, stride },
        #   conv2_active,
        #   conv2: { out_channels, kernel_size, stride, padding },
        #   pool_type,
        #   pool: { kernel_size, stride },
        #   fc_layers: [1, 4],
        #   fc_layer1: [64, 512],
        #   fc_layer2: [64, 512],
        #   fc_layer3: [64, 256],
        #   fc_layer4: [64, 256],
        #   optimizer,
        #   loss,
        #   batch_size,
        #   batch_normalization,
        #   learning_rate,
        #   weight_decay,
        #   dropout,
        #   beta1,
        #   beta2
        # }
        layers = []
        n_layers = config['fc_layers']
        in_channels = input_shape[0]
        self.batch_normalization = config['batch_normalization']
        c = nn.Conv2d(
            in_channels,
            config['conv1']['out_channels'],
            kernel_size=config['conv1']['kernel_size'],
            stride=config['conv1']['stride'],
            padding=config['conv1']['padding'])
        layers.append(c)
        if config['batch_normalization']:
            b = nn.BatchNorm2d(config['conv1']['out_channels'])
            layers.append(b)
        a = nn.ReLU(inplace=False)
        layers.append(a)
        # if config['pool1_type'] == 'max':
        #     p = nn.MaxPool2d(
        #         kernel_size=config['pool1']['kernel_size'],
        #         stride=config['pool1']['stride'])
        #     layers.append(p)
        # elif config['pool1_type'] == 'avg':
        #     p = nn.AvgPool2d(
        #         kernel_size=config['pool1']['kernel_size'],
        #         stride=config['pool1']['stride'])
        #     layers.append(p)

        in_channels = config['conv1']['out_channels']
        if config['conv2_active']:
            c = nn.Conv2d(
                in_channels,
                config['conv2']['out_channels'],
                kernel_size=config['conv2']['kernel_size'],
                stride=config['conv2']['stride'],
                padding=config['conv2']['padding'])
            layers.append(c)
            if self.batch_normalization:
                b = nn.BatchNorm2d(config['conv2']['out_channels'])
                layers.append(b)
            a = nn.ReLU(inplace=False)
            layers.append(a)
        if config['pool_type'] == 'max':
            p = nn.MaxPool2d(
                kernel_size=config['pool']['kernel_size'],
                stride=config['pool']['stride'])
            layers.append(p)
        elif config['pool_type'] == 'avg':
            p = nn.AvgPool2d(
                kernel_size=config['pool']['kernel_size'],
                stride=config['pool']['stride'])
            layers.append(p)

        self.conv_layers = nn.Sequential(*layers)
        self.output_size = num_classes

        self.fc_layers = nn.ModuleList()
        n_in = self._get_conv_output(input_shape)

        for i in range(1, n_layers + 1):
            n_out = config['fc_layer{}'.format(int(i))]
            fc = nn.Linear(int(n_in), int(n_out))
            self.fc_layers += [fc]
            if self.batch_normalization:
                self.fc_layers += [nn.BatchNorm1d(n_out)]
            n_in = n_out
        self.last_fc = nn.Linear(int(n_in), self.output_size)

        self.dropout = nn.Dropout(p=config['dropout'])

    # generate input sample and forward to get shape
    def _get_conv_output(self, shape):
        bs = 1
        input = Variable(torch.rand(bs, *shape))
        output_feat = self.conv_layers(input)
        n_size = output_feat.data.view(bs, -1).size(1)
        return n_size

    def forward(self, x):
        x = self.conv_layers(x)
        x = x.view(x.size(0), -1)
        index = 0
        while index < len(self.fc_layers):
        # for fc_layer in self.fc_layers:
            if self.batch_normalization and index + 1 < len(self.fc_layers):
                x = self.dropout(F.relu(self.fc_layers[index + 1](self.fc_layers[index](x))))
                index += 2
            else:
                x = self.dropout(F.relu(self.fc_layers[index](x)))
                index += 1
        x = self.last_fc(x)
        return x

    def train_fn(self, optimizer, criterion, loader, device, train=True, time_budget=None):
        """
        Training method
        :param optimizer: optimization algorithm
        :criterion: loss function
        :param loader: data loader for either training or testing set
        :param device: torch device
        :param train: boolean to indicate if training or test set is used
        :return: (accuracy, loss) on the data
        """
        t_start = time.time()
        score = AvgrageMeter()
        objs = AvgrageMeter()
        self.train()

        # t = tqdm(loader)
        iter_cnt = 0
        for images, labels in loader:  # t
            if time_budget:
                t_iteration = time.time()
                t_diff = t_iteration - t_start
                if t_diff >= time_budget:
                    break

            images = images.to(device)
            labels = labels.to(device)

            optimizer.zero_grad()
            logits = self(images)
            loss = criterion(logits, labels)
            loss.backward()
            optimizer.step()

            acc, _ = accuracy(logits, labels, topk=(1, 5))
            n = images.size(0)
            objs.update(loss.item(), n)
            score.update(acc.item(), n)

            iter_cnt += 1
            # t.set_description('(=> Training) Loss: {:.4f}'.format(objs.avg))

        return score.avg, objs.avg, 100.0 * iter_cnt / max(1, len(loader))

    def eval_fn(self, loader, device, train=False):
        """
        Evaluation method
        :param loader: data loader for either training or testing set
        :param device: torch device
        :param train: boolean to indicate if training or test set is used
        :return: accuracy on the data
        """
        score = AvgrageMeter()
        self.eval()

        # t = tqdm(loader)
        with torch.no_grad():  # no gradient needed
            for images, labels in loader:  # t
                images = images.to(device)
                labels = labels.to(device)

                outputs = self(images)
                acc, _ = accuracy(outputs, labels, topk=(1, 5))
                score.update(acc.item(), images.size(0))

                # t.set_description('(=> Test) Score: {:.4f}'.format(score.avg))

        return score.avg

