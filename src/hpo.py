# from main import main

import torch
import torch.nn

import ConfigSpace as CS


class Hyperparameters(object):

    def __init__(self):
        # === NAS hyperparameters ===
        self.nas = CS.ConfigurationSpace('NAS')
        # Conv 1
        conv1_kernel = CS.UniformIntegerHyperparameter('conv1_kernel', lower=2, upper=4)
        conv1_stride = CS.UniformIntegerHyperparameter('conv1_stride', lower=0, upper=2)  # 1, mid, max
        conv1_channels = CS.UniformIntegerHyperparameter('conv1_channels', lower=2, upper=8, log=False)
        self.nas.add_hyperparameters([conv1_kernel, conv1_stride, conv1_channels])
        # Pool 1
        # # pool1_type = CS.CategoricalHyperparameter('pool1_type', choices=[
        #     # 'none',
        #     'max', 'avg'])
        # # pool1_kernel = CS.UniformIntegerHyperparameter('pool1_kernel', lower=2, upper=4)
        # # pool1_stride = CS.UniformIntegerHyperparameter('pool1_stride', lower=0, upper=2)  # 1, mid, max
        # # self.nas.add_hyperparameters([pool1_type, pool1_kernel, pool1_stride])
        # self.nas.add_conditions([
        # #     CS.NotEqualsCondition(pool1_kernel_stride, pool1_type, 'none')
        # ])
        # Conv 2
        conv2_active = CS.CategoricalHyperparameter('conv2_active', choices=['no', 'yes'])
        conv2_kernel = CS.UniformIntegerHyperparameter('conv2_kernel', lower=2, upper=4)
        conv2_stride = CS.UniformIntegerHyperparameter('conv2_stride', lower=0, upper=2)  # 1, mid, max
        # conv2_channels = CS.UniformIntegerHyperparameter('conv2_channels', lower=2, upper=16, log=False)  # simply double
        self.nas.add_hyperparameters([conv2_active, conv2_kernel, conv2_stride])  # , conv2_channels])
        self.nas.add_conditions([
            CS.EqualsCondition(conv2_kernel, conv2_active, 'yes'),
            CS.EqualsCondition(conv2_stride, conv2_active, 'yes'),
        #     CS.EqualsCondition(conv2_channels, conv2_active, 'yes')
        ])
        # Pool 2
        pool_type = CS.CategoricalHyperparameter('pool_type', choices=[
            # 'none',
            'max', 'avg'])
        pool_kernel = CS.UniformIntegerHyperparameter('pool_kernel', lower=2, upper=4)
        pool_stride = CS.UniformIntegerHyperparameter('pool_stride', lower=0, upper=2)  # 1, mid, max
        self.nas.add_hyperparameters([pool_type, pool_kernel, pool_stride])
        # self.nas.add_conditions([
        #     CS.NotEqualsCondition(pool_kernel, conv2_active, 'no'),  # pool_type, 'none')
        #     CS.NotEqualsCondition(pool_stride, conv2_active, 'no')  # pool_type, 'none')
        # ])
        # Fully Connected Layers
        fc_layers = CS.UniformIntegerHyperparameter('fc_layers', lower=1, upper=3, log=False)   # upper=4
        fc_layer1_units = CS.UniformIntegerHyperparameter('fc_layer1_units', lower=49, upper=512, log=False)
        fc_layer2_units = CS.UniformIntegerHyperparameter('fc_layer2_units', lower=49, upper=512, log=False)
        fc_layer3_units = CS.UniformIntegerHyperparameter('fc_layer3_units', lower=49, upper=256, log=False)
        # fc_layer4_units = CS.UniformIntegerHyperparameter('fc_layer4_units', lower=49, upper=256, log=False)
        self.nas.add_hyperparameters([
            fc_layers, fc_layer1_units, fc_layer2_units, fc_layer3_units  # , fc_layer4_units
        ])
        self.nas.add_conditions([
            CS.GreaterThanCondition(fc_layer2_units, fc_layers, 1),
            CS.GreaterThanCondition(fc_layer3_units, fc_layers, 2),
            # CS.GreaterThanCondition(fc_layer4_units, fc_layers, 3)
        ])
        # === Non-Architectural HPs ===
        self.nnopt = CS.ConfigurationSpace()
        # optimizer
        optimizer = CS.CategoricalHyperparameter('optimizer', choices=['adam', 'adad', 'sgd'])
        loss = CS.CategoricalHyperparameter('loss', choices=['cross_entropy', 'mse'])
        batch_size = CS.UniformIntegerHyperparameter('batch_size', lower=48, upper=192, log=True)  # default: 96
        batch_normalization = CS.CategoricalHyperparameter('batch_normalization', choices=['no', 'yes'])  # default: no
        learning_rate = CS.UniformFloatHyperparameter('learning_rate', lower=0.0005, upper=2.0, log=True)
        weight_decay = CS.UniformFloatHyperparameter('weightdecay', lower=0.05, upper=0.4, log=True)  # weightdecay -= 0.10, min 0.0
        dropout = CS.UniformFloatHyperparameter('dropout', lower=0, upper=0.7, log=False)
        # adam
        #   default learning rate = 0.01
        #   weight_decay
        #   default beta1 = 0.9
        #   default beta2 = 0.999
        beta1_inv = CS.UniformFloatHyperparameter('beta1_inv', lower=0.0001, upper=0.3, log=True)  # beta1 = 1 - beta1_inv
        beta2_inv = CS.UniformFloatHyperparameter('beta2_inv', lower=0.0001, upper=0.3, log=True)  # beta2 = 1 - beta2_inv
        # adadelta
        #   default learning rate = 1.0
        #   weight_decay
        #   rho_inv = CS.UniformFloatHyperparameter('rho_inv', lower=0.001, upper=0.4, log=True)  # rho = 1 - rho_inv
        #     use beta1_inv for rho_inv
        # SGD
        #   default learning rate = 0.001
        #   weight_decay
        self.nnopt.add_hyperparameters([optimizer, loss, batch_size, batch_normalization, learning_rate, weight_decay, dropout, beta1_inv, beta2_inv])
        self.nnopt.add_conditions([
            CS.InCondition(beta1_inv, optimizer, ['adam', 'adad']),
            CS.EqualsCondition(beta2_inv, optimizer, 'adam')
        ])

    @staticmethod
    def get_hp_dict(nas_hps, nn_hps):
        loss_dict = {'cross_entropy': torch.nn.CrossEntropyLoss,
                     'mse': torch.nn.MSELoss}
        opti_dict = {'adam': torch.optim.Adam,
                     'adad': torch.optim.Adadelta,
                     'sgd': torch.optim.SGD}

        hps = nas_hps
        conv1_channels = hps.get('conv1_channels', 4)
        conv1_kernel = hps.get('conv1_kernel', 2)
        conv1_stride = hps.get('conv1_stride', 2)
        # pool1_kernel = hps.get('pool1_kernel', 2)
        # pool1_stride = hps.get('pool1_stride', 0)
        conv2_active = (hps.get('conv2_active', 'no') == 'yes')
        conv2_kernel = hps.get('conv2_kernel', 2)
        conv2_stride = hps.get('conv2_stride', 2)
        pool_kernel = hps.get('pool_kernel', 2)
        pool_stride = hps.get('pool_stride', 0)

        hp_dict = {
            # === ARCHITECTURE HPS ===
            'conv1': {
                'out_channels': conv1_channels,
                'kernel_size': conv1_kernel,
                'stride': int(1 + (conv1_kernel - 1) * conv1_stride / 2),
                'padding': int(conv1_kernel / 2)
            },
            # 'pool1_type': hps.get('pool1_type', 'max'),
            # 'pool1': {
                # 'kernel_size': pool1_kernel,
                # 'stride': int(1 + (pool1_kernel - 1) * pool1_stride / 2)
            # },
            'conv2_active': conv2_active,
            'conv2': {
               'out_channels': int(conv1_channels * 1.7),
               'kernel_size': conv2_kernel,
               'stride': int(1 + (conv2_kernel - 1) * conv2_stride / 2),
               'padding': int(conv2_kernel / 2)
            },
            'pool_type': hps.get('pool_type', 'max'),
            'pool': {
               'kernel_size': pool_kernel,
               'stride': int(1 + (pool_kernel - 1) * pool_stride / 2)
            },
            'fc_layers': int(hps.get('fc_layers', 1)),
            'fc_layer1': int(hps.get('fc_layer1_units', 256)),
            'fc_layer2': int(hps.get('fc_layer2_units', 256)),
            'fc_layer3': int(hps.get('fc_layer3_units', 128)),
            'fc_layer4': int(hps.get('fc_layer4_units', 128)),
            # === NN HPs ===
            'optimizer': opti_dict[nn_hps.get('optimizer', 'adam')],
            'loss': loss_dict[nn_hps.get('loss', 'cross_entropy')],
            'batch_size': int(nn_hps.get('batch_size', 96)),
            'batch_normalization': (nn_hps.get('batch_normalization', 'yes') == 'yes'),
            'learning_rate': float(nn_hps.get('learning_rate', 0.01)),
            'weight_decay': max(0.0, float(nn_hps.get('weight_decay', 0.05)) - 0.1),
            'dropout': float(nn_hps.get('dropout', 0.3)),  # original: 0.2
            'beta1': 1.0 - float(nn_hps.get('beta1_inv', 0.1)),
            'beta2': 1.0 - float(nn_hps.get('beta2_inv', 0.001)),
        }
        return hp_dict


class HPO(object):

    def __init__(self):
        pass

