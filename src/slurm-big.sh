#!/bin/bash

#SBATCH -p meta_gpu-black # partition (queue)
#SBATCH --mem 4000 # memory pool for all cores (6GB)
#SBATCH -t 0-00:00 # time (D-HH:MM)
#SBATCH -c 4 # number of cores
#SBATCH --gres=gpu:4
#SBATCH -o log/%x.%N.%j.out # STDOUT  (the folder log has to be created prior to running or this won't work)
#SBATCH -e log/%x.%N.%j.err # STDERR  (the folder log has to be created prior to running or this won't work)
#SBATCH -J cnnopt_big # sets the job name. If not specified, the file name will be used as job name
#SBATCH -D /home/kimmiga/pro/src/src # sets the working directory

#SBATCH --mail-type=END,FAIL # (receive mails about end and timeouts/crashes of your job)
# Print some information about the job to STDOUT
echo "Workingdir: $PWD";
echo "Started at $(date)";
echo "Running job $SLURM_JOB_NAME using $SLURM_JOB_CPUS_PER_NODE cpus per node with given JID $SLURM_JOB_ID on queue $SLURM_JOB_PARTITION";

# Job to perform
python3.6 bohb.py --id 'big' --min_budget 120.0 --max_budget 720.0 --n_iterations 24 --n_workers 4 --num_samples 128 --random_fraction 0.25 --eta 3.0

# Print some Information about the end-time to STDOUT
echo "DONE";
echo "Finished at $(date)";
