import logging
import sys
from datetime import datetime
import argparse

cmdline_parser = argparse.ArgumentParser('BOHB')

cmdline_parser.add_argument('--id', default='', help='Identifier String', type=str)
cmdline_parser.add_argument('--min_budget', default=180.0, type=float)
cmdline_parser.add_argument('--max_budget', default=1800.0, type=float)
cmdline_parser.add_argument('--n_iterations', default=20, type=int)
cmdline_parser.add_argument('--n_workers', default=4, type=int)
cmdline_parser.add_argument('--num_samples', default=32, type=int)
cmdline_parser.add_argument('--random_fraction', default=0.25, type=float)
cmdline_parser.add_argument('--eta', default=2.5, type=float)
args, unknowns = cmdline_parser.parse_known_args()
id_str = args.id
min_budget = args.min_budget
max_budget = args.max_budget
n_iterations = args.n_iterations
n_workers = args.n_workers
num_samples = args.num_samples
random_fraction = args.random_fraction
eta = args.eta

import hpbandster.core.nameserver as hpns
import hpbandster.core.result as hpres

from hpbandster.optimizers import BOHB as BOHB
from hpbandster.core.worker import Worker

from hpbandster.examples.commons import MyWorker

from ConfigSpace import ConfigurationSpace

from main import main

import hpo

logger = logging.getLogger()
formatter = logging.Formatter('%(asctime)s [%(levelname)-5.5s]  %(message)s')
fh = logging.FileHandler('ign_bohb_{}_master.log'.format(id_str))
fh.setFormatter(formatter)
logger.addHandler(fh)
sh = logging.StreamHandler(sys.stdout)
sh.setFormatter(formatter)
logger.addHandler(sh)
logger.setLevel(logging.INFO)

hps = hpo.Hyperparameters()
arch_configspace = hps.nas
opt_configspace = hps.nnopt
# full_configspace = ConfigurationSpace()
# full_configspace.add_configuration_space(prefix='', configuration_space=arch_configspace)
# full_configspace.add_configuration_space(prefix='', configuration_space=opt_configspace)
best_arch_parameters = [(ConfigurationSpace().sample_configuration(), {'loss': 100.0})]  # 1-based
best_opt_parameters = [(ConfigurationSpace().sample_configuration(), {'loss': 100.0})]  # 1-based


class CNNWorker(Worker):

    def __init__(self, *args, id=None, **kwargs):
        super().__init__(*args, id=id, **kwargs)
        self.id = id

    @staticmethod
    def get_configspace():
        return arch_configspace

    def compute(self, config_id, config, budget, **kwargs):
        arch_parameters = config
        best_opt_parameters_now = best_opt_parameters[-1][0]
        model_hps = hpo.Hyperparameters.get_hp_dict(arch_parameters, best_opt_parameters_now)
        iteration = config_id[0]
        complete_id = '{}_{}_{}'.format(config_id[0], config_id[1], config_id[2])
        # datetime.today().strftime('%Y-%m-%d_%H-%M-%S.%f')
        try:
            _res = main(
                model_hps,
                time_budget=budget,
                data_dir='../data',
                num_epochs=20,
                data_augmentations=None,
                save_model_str='ign_cnn_opt_bohb_{}__{}.model'.format(id_str, complete_id),
                log_file='ign_cnn_opt_bohb_{}__{}.log'.format(id_str, complete_id)
            )
        except Exception as e:
            _res = {
                'test_score': 0.0
            }

        res = {
            'loss': float(100 - _res['test_score']),
            'info': str({
                'hyperparameters': model_hps,
                'test_scores': _res['test_scores'],
                'id_str': id_str,
                'id': complete_id
            })
        }
        iteration += 1  # make iteration 1-based
        if len(best_arch_parameters) <= iteration:
            best_arch_parameters.append((arch_parameters, res))
            logger.info('Results for new BEST ARCH parameters: {}'.format(str(res)))
        elif best_arch_parameters[iteration][1]['loss'] > res['loss']:
            best_arch_parameters[iteration] = (arch_parameters, res)
            logger.info('Result for new BEST ARCH parameters: {}'.format(str(res)))
        else:
            logger.info('Result for new ARCH parameters: {}'.format(str(res)))
        return res


class OptWorker(Worker):

    def __init__(self, *args, id=None, **kwargs):
        super().__init__(*args, id=id, **kwargs)
        self.id = id

    @staticmethod
    def get_configspace():
        return opt_configspace

    def compute(self, config_id, config, budget, **kwargs):
        opt_parameters = config
        best_arch_parameters_now = best_arch_parameters[-1][0]
        model_hps = hpo.Hyperparameters.get_hp_dict(best_arch_parameters_now, opt_parameters)

        iteration = config_id[0]
        complete_id = '{}_{}_{}'.format(config_id[0], config_id[1], config_id[2])
        # datetime.today().strftime('%Y-%m-%d_%H-%M-%S.%f')
        try:
            _res = main(
                model_hps,
                time_budget=budget,
                data_dir='../data',
                num_epochs=20,
                data_augmentations=None,
                save_model_str='ign_opt_opt_bohb_{}__{}.model'.format(id_str, complete_id),
                log_file='ign_opt_opt_bohb_{}__{}.log'.format(id_str, complete_id)
            )
        except Exception as e:
            _res = {
                'test_score': 0.0
            }

        res = {
            'loss': float(100 - _res['test_score']),
            'info': str({
                'hyperparameters': model_hps,
                'test_scores': _res['test_scores'],
                'id_str': id_str,
                'id': complete_id
            })
        }
        iteration += 1  # make iteration 1-based
        if len(best_opt_parameters) <= iteration:
            best_opt_parameters.append((opt_parameters, res))
            logger.info('Results for new BEST OPT parameters: {}'.format(str(res)))
        elif best_opt_parameters[iteration][1]['loss'] > res['loss']:
            best_opt_parameters[iteration] = (opt_parameters, res)
            logger.info('Result for new BEST OPT parameters: {}'.format(str(res)))
        else:
            logger.info('Result for new OPT parameters: {}'.format(str(res)))
        return res


def bohb_main():

    # min_budget = float(60 * 3)
    # max_budget = float(60 * 30)
    # n_iterations = int(20)
    # n_workers = int(4)
    # num_samples = 32
    # random_fraction = 0.25
    # eta = 2.5

    run_id = 'ak_cnn_bohb_{}'.format(id_str)

    # Step 1: Start a nameserver (see example_1)
    NS = hpns.NameServer(run_id=run_id, host='127.0.0.1', port=None)
    NS.start()

    # Step 2: Start the workers
    # Now we can instantiate the specified number of workers. To emphasize the effect,
    # we introduce a sleep_interval of one second, which makes every function evaluation
    # take a bit of time. Note the additional id argument that helps separating the
    # individual workers. This is necessary because every worker uses its processes
    # ID which is the same for all threads here.
    workers=[]
    for i in range(n_workers):
        w = CNNWorker(nameserver='127.0.0.1', run_id=run_id, id=i)
        w.run(background=True)
        workers.append(w)

    # Step 3: Run an optimizer
    # Now we can create an optimizer object and start the run.
    # We add the min_n_workers argument to the run methods to make the optimizer wait
    # for all workers to start. This is not mandatory, and workers can be added
    # at any time, but if the timing of the run is essential, this can be used to
    # synchronize all workers right at the start.
    bohb = BOHB(  configspace=CNNWorker.get_configspace(),
                  run_id=run_id,
                  min_budget=min_budget, max_budget=max_budget,
                  random_fraction=random_fraction, eta=eta,
                  num_samples=num_samples
               )
    res = bohb.run(n_iterations=n_iterations, min_n_workers=n_workers)

    # Step 4: Shutdown
    # After the optimizer run, we must shutdown the master and the nameserver.
    bohb.shutdown(shutdown_workers=True)

    # Step 5: Analysis
    # Each optimizer returns a hpbandster.core.result.Result object.
    # It holds informations about the optimization run like the incumbent (=best) configuration.
    # For further details about the Result object, see its documentation.
    # Here we simply print out the best config and some statistics about the performed runs.
    id2config = res.get_id2config_mapping()
    incumbent = res.get_incumbent_id()

    all_runs = res.get_all_runs()

    logger.info('Best found configuration:', id2config[incumbent]['config'])
    logger.info('A total of %i unique configurations where sampled.' % len(id2config.keys()))
    logger.info('A total of %i runs where executed.' % len(res.get_all_runs()))
    logger.info('Total budget corresponds to %.1f full function evaluations.'%(sum([r.budget for r in all_runs])/max_budget))
    logger.info('Total budget corresponds to %.1f full function evaluations.'%(sum([r.budget for r in all_runs])/max_budget))
    logger.info('The run took  %.1f seconds to complete.'%(all_runs[-1].time_stamps['finished'] - all_runs[0].time_stamps['started']))

    # now, do optimizer HPO

    # min_budget = float(60 * 3)
    # max_budget = float(60 * 30)
    # n_iterations = int(20)
    # n_workers = int(4)
    # num_samples = 32
    # random_fraction = 0.25
    # eta = 2.5

    # Step 2: Start the workers
    # Now we can instantiate the specified number of workers. To emphasize the effect,
    # we introduce a sleep_interval of one second, which makes every function evaluation
    # take a bit of time. Note the additional id argument that helps separating the
    # individual workers. This is necessary because every worker uses its processes
    # ID which is the same for all threads here.
    workers=[]
    for i in range(n_workers):
        w = OptWorker(nameserver='127.0.0.1', run_id=run_id, id=i)
        w.run(background=True)
        workers.append(w)

    # Step 3: Run an optimizer
    # Now we can create an optimizer object and start the run.
    # We add the min_n_workers argument to the run methods to make the optimizer wait
    # for all workers to start. This is not mandatory, and workers can be added
    # at any time, but if the timing of the run is essential, this can be used to
    # synchronize all workers right at the start.
    bohb = BOHB(  configspace=OptWorker.get_configspace(),
                  run_id=run_id,
                  min_budget=min_budget, max_budget=max_budget,
                  random_fraction=random_fraction, eta=eta,
                  num_samples=num_samples
               )
    res = bohb.run(n_iterations=n_iterations, min_n_workers=n_workers)

    # Step 4: Shutdown
    # After the optimizer run, we must shutdown the master and the nameserver.
    bohb.shutdown(shutdown_workers=True)
    NS.shutdown()

    # Step 5: Analysis
    # Each optimizer returns a hpbandster.core.result.Result object.
    # It holds informations about the optimization run like the incumbent (=best) configuration.
    # For further details about the Result object, see its documentation.
    # Here we simply print out the best config and some statistics about the performed runs.
    id2config = res.get_id2config_mapping()
    incumbent = res.get_incumbent_id()

    all_runs = res.get_all_runs()

    logger.info('Best found configuration:', id2config[incumbent]['config'])
    logger.info('A total of %i unique configurations where sampled.' % len(id2config.keys()))
    logger.info('A total of %i runs where executed.' % len(res.get_all_runs()))
    logger.info('Total budget corresponds to %.1f full function evaluations.'%(sum([r.budget for r in all_runs])/max_budget))
    logger.info('Total budget corresponds to %.1f full function evaluations.'%(sum([r.budget for r in all_runs])/max_budget))
    logger.info('The run took  %.1f seconds to complete.'%(all_runs[-1].time_stamps['finished'] - all_runs[0].time_stamps['started']))


if __name__ == '__main__':
    import atexit
    import traceback
    @atexit.register
    def atexit_func():
        logger.info('Exiting BOHB ...')
    try:
        bohb_main()
    except Exception as e:
        logger.info(traceback.format_exc())
    except BaseException as e:
        logger.info(traceback.format_exc())
