import os
import argparse
import logging
import time
import numpy as np
import sys
import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from summary import summary

from ConfigSpace import ConfigurationSpace

from cnn import torchModel
from datasets import K49
import hpo


# initialize seeds
torch.manual_seed(183)
np.random.seed(None)


def main(model_hps,
         time_budget=None,
         data_dir=None,
         num_epochs=10,
         #batch_size=50,
         #learning_rate=0.001,
         #train_criterion=torch.nn.CrossEntropyLoss,
         #model_optimizer=torch.optim.Adam,
         data_augmentations=None,
         save_model_str=None,
         log_file=None):
    """
    Training loop for configurableNet.
    :param model_hps: network config (dict)
    :param data_dir: dataset path (str)
    :param num_epochs: (int)
    :param batch_size: (int)
    :param learning_rate: model optimizer learning rate (float)
    :param train_criterion: Which loss to use during training (torch.nn._Loss)
    :param model_optimizer: Which model optimizer to use during trainnig (torch.optim.Optimizer)
    :param data_augmentations: List of data augmentations to apply such as rescaling.
        (list[transformations], transforms.Composition[list[transformations]], None)
        If none only ToTensor is used
    :return:
    """

    formatter = logging.Formatter('%(asctime)s [%(levelname)-5.5s]  %(message)s')
    if log_file:
        logger = logging.Logger(name=log_file)
        fh = logging.FileHandler(log_file)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
    else:
        logger = logging.getLogger()
    sh = logging.StreamHandler(sys.stdout)
    sh.setFormatter(formatter)
    logger.addHandler(sh)
    logger.setLevel(logging.INFO)

    batch_size = model_hps['batch_size']
    learning_rate = model_hps['learning_rate']
    train_criterion = model_hps['loss']
    model_optimizer = model_hps['optimizer']

    # Device configuration
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    if data_augmentations is None:
        # You can add any preprocessing/data augmentation you want here
        data_augmentations = transforms.ToTensor()
    elif isinstance(type(data_augmentations), list):
        data_augmentations = transforms.Compose(data_augmentations)
    elif not isinstance(data_augmentations, transforms.Compose):
        raise NotImplementedError

    train_dataset = K49(data_dir, True, data_augmentations)
    test_dataset = K49(data_dir, False, data_augmentations)

    # Make data batch iterable
    # Could modify the sampler to not uniformly random sample
    train_loader = DataLoader(dataset=train_dataset,
                              batch_size=batch_size,
                              shuffle=True)
    test_loader = DataLoader(dataset=test_dataset,
                             batch_size=batch_size,
                             shuffle=False)

    model = torchModel(model_hps,
                       input_shape=(
                           train_dataset.channels,
                           train_dataset.img_rows,
                           train_dataset.img_cols
                       ),
                       num_classes=train_dataset.n_classes
                       ).to(device)
    total_model_params = np.sum(p.numel() for p in model.parameters())
    # instantiate optimizer
    optimizer = model_optimizer(model.parameters(),
                                lr=learning_rate)
    # instantiate training criterion
    train_criterion = train_criterion().to(device)

    logger.info('Hyperparameters:{}'.format(model_hps))
    logger.info('Generated Network:')
    summary_model = summary(model, (train_dataset.channels,
                    train_dataset.img_rows,
                    train_dataset.img_cols),
            device='cuda' if torch.cuda.is_available() else 'cpu')
    logger.info(summary_model[0])
    # if summary_model[1] > 330000 or summary_model[1] < 100000:  # don't allow models with more than 330000 parameters or less than 100000 parameters
    #     return {
    #         'test_score': 0.0,
    #         'test_scores': []
    #     }
    logger.info('Learning Parameters:\n{}'.format('\n'.join([
        'learning_rate: {}'.format(learning_rate),
        'optimizer: {}'.format(optimizer),
        'num_epochs: {}'.format(num_epochs),
        'batch_size: {}'.format(batch_size),
        'train_criterion: {}'.format(train_criterion),
    ])))

    # Train the model
    t_start = time.time()
    t_eval = 20  # evaluation takes approx 20 seconds (a bit hacky)
    best_test_score = 0
    test_score = 0
    test_scores = []
    completed = True
    for epoch in range(num_epochs):
        epoch_budget = None
        if time_budget:
            t_epoch = time.time()
            t_diff = t_epoch - t_start
            epoch_budget = time_budget - t_diff - t_eval
            if epoch_budget <= 0:
                break

        logger.info('#' * 50)
        logger.info('Epoch [{}/{}]'.format(epoch + 1, num_epochs))

        train_score, train_loss, percentage_finished = model.train_fn(optimizer, train_criterion, train_loader, device, time_budget=epoch_budget)
        if percentage_finished != 100.0:
            completed = False
            logger.info('Finished {}% of training'.format(percentage_finished))
        logger.info('Train loss %f', train_loss)
        logger.info('Train accuracy %f', train_score)

        test_score = model.eval_fn(test_loader, device)
        logger.info('Test accuracy %f', test_score)
        test_scores.append(test_score)
        if test_score > best_test_score:
            best_test_score = test_score
        else:
            # stop training if test score did not improve
            break

    logger.info('Test Scores: {}'.format(test_scores))
    logger.info('Completed: {}'.format(completed))

    if save_model_str:
        # Save the model checkpoint can be restored via "model = torch.load(save_model_str)"
        while os.path.exists(save_model_str):
            save_model_str += '_'
        torch.save(model.state_dict(), save_model_str)

    return {
        'test_score': test_score,
        'test_scores': test_scores
    }


if __name__ == '__main__':
    """
    This is just an example of how you can use train and evaluate
    to interact with the configurable network
    """
    loss_dict = {'cross_entropy': torch.nn.CrossEntropyLoss,
                 'mse': torch.nn.MSELoss}
    opti_dict = {'adam': torch.optim.Adam,
                 'adad': torch.optim.Adadelta,
                 'sgd': torch.optim.SGD}

    cmdline_parser = argparse.ArgumentParser('AutoML SS19 final project')

    cmdline_parser.add_argument('-e', '--epochs',
                                default=10,
                                help='Number of epochs',
                                type=int)
    cmdline_parser.add_argument('-b', '--batch_size',
                                default=96,
                                help='Batch size',
                                type=int)
    cmdline_parser.add_argument('-D', '--data_dir',
                                default='../data',
                                help='Directory in which the data is stored (can be downloaded)')
    cmdline_parser.add_argument('-l', '--learning_rate',
                                default=0.01,
                                help='Optimizer learning rate',
                                type=float)
    cmdline_parser.add_argument('-L', '--training_loss',
                                default='cross_entropy',
                                help='Which loss to use during training',
                                choices=list(loss_dict.keys()),
                                type=str)
    cmdline_parser.add_argument('-o', '--optimizer',
                                default='adam',
                                help='Which optimizer to use during training',
                                choices=list(opti_dict.keys()),
                                type=str)
    cmdline_parser.add_argument('-m', '--model_path',
                                default=None,
                                help='Path to store model',
                                type=str)
    args, unknowns = cmdline_parser.parse_known_args()

    if unknowns:
        logging.warning('Found unknown arguments!')
        logging.warning(str(unknowns))
        logging.warning('These will be ignored')

    # architecture parametrization and NN hyperparameters
    nullConfiguration = ConfigurationSpace().sample_configuration()
    model_hps = hpo.Hyperparameters.get_hp_dict(nullConfiguration, nullConfiguration)

    main(
        model_hps,
        time_budget=None,
        data_dir=args.data_dir,
        num_epochs=args.epochs,
        #batch_size=args.batch_size,
        #learning_rate=args.learning_rate,
        #train_criterion=loss_dict[args.training_loss],
        #model_optimizer=opti_dict[args.optimizer],
        data_augmentations=None,
        save_model_str=args.model_path,
        log_file=None
    )
