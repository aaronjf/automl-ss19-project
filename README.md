# README #

## RUN ##

Launch with sbatch:

```
> sbatch slurm*.sh
```

Run BOHB manually:

```
> python3.6 bohb.py --id 'normal' --min_budget 180.0 --max_budget 1800.0 --n_iterations 20 --n_workers 4 --num_samples 32 --random_fraction 0.25 --eta 2.5
```

## LOGS and MODELS ##

`./log/*/*.log` and `./log/*/*.model`
